import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by u5342011 on 26/02/16.
 */
public class lotus {

    public static int max(int[] array){
        int maximum = array[0];
        for(int i = 0; i < array.length; i++){
            if(array[i] > maximum){
                maximum = array[i];
            }
        }
        return maximum;
    }

    public static int min(int[] array){
        int minimum = array[0];
        for(int i = 0; i < array.length; i++){
            if(array[i] < minimum){
                minimum = array[i];
            }
        }
        return minimum;
    }


    public static void main(String[] args) {
        int size = 100 ;
        int limit = 100 ;
        int[] array = new int[size];
        Random generator = new Random();

        for(int i = 0; i < size; i++){
            array[i] = generator.nextInt(limit);
            System.out.println(array[i]);
        }

        System.out.println(max(array));
        System.out.println(min(array));
    }
}
